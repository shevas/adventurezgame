#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
path = os.path.dirname(os.path.abspath(sys.argv[0]))
import importlib.util
import importlib.machinery


def load_module(name,fpath):
  fullpath = fpath+"/Addons/Addon1.py"
  print("FULLPATH: {}".format(fullpath))
  loader = importlib.machinery.SourceFileLoader(name, fullpath)
  spec = importlib.util.spec_from_loader(loader.name, loader)
  mod = importlib.util.module_from_spec(spec)
  loader.exec_module(mod)
  return mod


try:
  addon1 = load_module("Addon1", path)
  found = True
except ImportError as e:
  found = False
  print("ERROR? {}".format(e))
import random
import argparse
import json
from random import randint

if(found==True):
  print ("Dodatek 1 zostal zaladowany!")
save_file=""
class Person:
  def __init__(myself):
    myself.name = ""
    myself.health = 1
    myself.health_max = 1
  def inflict_dmg(myself, enemy):
    damage = min(
        max(randint(0, myself.health) - randint(0, enemy.health), 0),
        enemy.health)
    enemy.health = enemy.health - damage
    if damage == 0:
      print ("%s unika ataku!\n" % (enemy.name))
    else: print ("%s raniony z siłą -%dhp \n" % (enemy.name, damage))
    return enemy.health <= 0

class Enemy(Person):
  def __init__(myself, player, myname):
    Person.__init__(myself)
    ex = randint (int(player.health_max*0.5)-2, int(player.health_max*0.75))
    if myname=="":
        enemy_name1 = randint(0, 6)
        if enemy_name1 == 0:
          en = "Goblin"
          eh = ex
        elif enemy_name1 == 1:
          en = "Szczur"
          eh = ex -1
        elif enemy_name1 == 2:
          en = "Potwór z bagien"
          eh = ex +1
        elif enemy_name1 == 3:
          en = "Czarny rycerz"
          eh = ex +2
        elif enemy_name1 == 4:
          en = "Wilkołak"
          eh = ex +3
        elif enemy_name1 == 5:
          en = "Bogin"
          eh = ex +4
        elif enemy_name1 == 6:
          en = "Wściekły królik"
          eh = ex -1
    else:
      en = myname
      eh = ex +5
    myself.name = "%s"%(en)
    myself.health = eh

class Player(Person):
  def __init__(myself):
    Person.__init__(myself)
    myself.state = 'normal'
    myself.merchstate = 'normal'
    myself.health = 10
    myself.health_max = 10
    myself.loot = 10
    myself.place = ""
    myself.aid = 3
    myself.ammo = 15
    myself.currloc = ""
    myself.randomizer = 3
    myself.enemies_defeated = 0
  def quit(myself):
    print ("%s ucieka z Adwenturni z przerażeniem... Co za tchórz! \n" % myself.name)
    myself.health = 0
  def toJson(myself):
    print ("Jak chcesz nazwać plik z zapisem gry?")
    filename = input()
    with open(filename,'w+') as outfile:
      return json.dump({'name':myself.name, 'health':myself.health, 'max_health':myself.health_max, 'money':myself.loot,
      'aid':myself.aid, 'ammo':myself.ammo, 'merch':myself.merchstate}, outfile, sort_keys='true', indent=4)
  def help(myself): print ("Dostępne komedy:\n\
  'pomoc': Wypisuje dostępne komendy\n\
  'wyjdz': Wychodzi z gry\n\
  'zapisz': Zapisuje obecny stan gry do pliku\n\
  'status': Pokazuje status Twojego bohatera (Życie, Kasa, Apteczki, Amunicja)\n\
  'zwiedzaj': Kontynuujesz swoją przygodę zwiedzając lokalizację, w której jesteś\n\
  'przeszukaj': (OPCJA DOSTĘPNA TYLKO Z DODATKIEM Addon1!)Przeszukujesz dana lokalizacje, w ktorej jestes\n\
  'atakuj': Atakujesz potwora(podczas walki), strzelasz w nicość (nie podczas walki)\n\
  'odpocznij': Odpoczywasz, żeby odzyskać energię\n\
  'uciekaj': Starasz się uciec od atakującego Cię potwora, albo biegasz w kółko\n\
  'kup1': Kup pierwszy przedmiot u sprzedawcy\n\
  'kup2': Kup drugi przedmiot u sprzedawcy\n\
  'apteczka': Użyj apteczki\n\
  'sprzedaj': Sprzedaj coś ze swoich przedmiotów, żeby dostać kasę\n")
  def status(myself): print ("%s\n Życie: %d/%d\n Kasa:%d Apteczki:%d Amunicja:%d" % (myself.name, myself.health, myself.health_max, myself.loot, myself.aid, myself.ammo))
  def oofammo(myself):
    if myself.ammo <= 0:
      print ("Bez amunicji nic tu po mnie...\n")
      myself.health = 0   
  def item1(myself):
    if myself.merchstate == 'buy' :
      if myself.merchaid > 0:
        if myself.loot >=20:
          myself.aid = myself.aid +1
          myself.loot = myself.loot - 20
          myself.merchaid = myself.merchaid -1
          print("%s kupił 1 apteczkę" % myself.name)
        else: print ("Za biednyś!")
      else: print ("Sprzedawca nie ma obecnie żadnej apteczki")
    else:
      print ("W sklepie remanent - sprzedawca zajęty")
      myself.merchstate = 'normal'
  def item2(myself):
    if myself.merchstate == 'buy' :
      if myself.merchammo > 0:
        if myself.loot >= 10:
          myself.ammo = myself.ammo + 5
          myself.loot = myself.loot - 10
          myself.merchammo = myself.merchammo - 5
          print ("%s kupił 5 naboi" % myself.name)
        else: print ("Za biednyś!")
      else: print ("Sprzedawca nie ma obecnie żadnych naboi na sprzedaż")
    else:
      print ("Sprzedawca zapił na imprezie. Sklep zamknięty")
      myself.merchstate = 'normal'
  def sell(myself):
    if myself.merchstate == 'buy' :
      if myself.aid > 0:
          myself.aid = myself.aid -1
          myself.loot = myself.loot + 10
          myself.merchaid = myself.merchaid +1
          print ("%s odsprzedał 1 apteczkę" % myself.name)
      else: print ("%s nie ma czego sprzedawać!" % myself.name)
    else:
      print ("Sprzedawca spóźnia się - sklep powinien być już otwarty!")
      myself.merchstate = 'normal'
  def aid(myself):
    if myself.aid > 0:
      if myself.health < myself.health_max:
        myself.health = myself.health + 2
        myself.aid = myself.aid -1
        print ("Używasz apteczki\n +2 życia")
        if myself.health > myself.health_max:
          myself.health = myself.health_max
      else: print ("Jesteś zdrów jak ryba!")
    else: print ("Nie masz żadnych apteczek")
  def merch(myself):
    myself.merchstate = 'buy'
    myself.merchammo = 0
    myself.merchaid = 0
    myself.merchaid = myself.merchaid + randint(0,10)
    myself.merchammo = myself.merchammo + randint(0, 50)
    print ("%s pojawił się sprzedawca!" % myself.name)
    print ("Ma dziś na sprzedaż:\n Apteczki:%d\n Amunicja:%d" % (myself.merchaid, myself.merchammo))
  def tired(myself):
    print ("%s się straaaaasznie zmęczył. Trzeba by odpocząć" % myself.name)
    myself.health = max(1, myself.health - 1)
  def rest(myself):
    if myself.state != 'normal': print ("%s sobie teraz nie pośpi..." % myself.name); myself.enemy_attacks()
    else:
      print ("%s idzie w kimę" % myself.name)
      if randint(0, 1):
        myself.enemy = Enemy(myself,"")
        print ("Do jasnej ciasnej! Jakiś głupi %s z %s życia Cię obudził!" % (myself.enemy.name,myself.enemy.health))
        myself.state = 'fight'
        myself.enemy_attacks()
      else:
        if myself.health < myself.health_max:
          myself.health = myself.health + 1
        else: print ("%s rozespał się i słabo się teraz czuje. Życie -1" % myself.name); myself.health = myself.health - 1
  def places(myself,currplace):
      area = randint(0,3)
      place = ""
      if currplace == "":
        place = "Grotę Roweckiego"
      else:
        place = currplace
      if area==0:
        place = "Grotę Roweckiego"
      elif area==1:
        place = "Zakazany Las"
      elif area==2:
        place = "Opuszczone Miasto"
      return place
  def explore(myself):
    myself.oofammo()
    myself.merchstate = 'normal'
    if myself.state != 'normal':
      print ("Jesteś zajęty czymś innym!")
      myself.enemy_attacks()
    elif myself.ammo > 0:
      myself.place = myself.places(myself.place)
      print ("%s zwiedza %s" % (myself.name, myself.place))
      if randint(0, 1):
        myself.enemy = Enemy(myself,"")
        print ("%s napotyka na %s!\n Życie Przeciwnika: %d." % (myself.name, myself.enemy.name, myself.enemy.health))
        myself.state = 'fight'
      else:
        if randint(0, 1): myself.tired()
        if randint(0, 1): myself.merch()
  def flee(myself):
    if myself.state != 'fight': print ("%s biegasz w kółko bez sensu..." % myself.name); myself.tired()
    else:
      if randint(1, myself.health + 5) > randint(1, myself.enemy.health):
        print ("%s uciekł od %s. Uff!" % (myself.name, myself.enemy.name))
        myself.enemy = None
        myself.state = 'normal'
      else: print ("%s próbuje uciec od %s, ale z marnym skutkiem!" % (myself.name, myself.enemy.name)); myself.enemy_attacks()
  def attack(myself):
    myself.oofammo()
    myself.ammo = myself.ammo - 1
    if myself.state != 'fight': 
      if myself.ammo > 0: print ("%s strzela gdzieś w powietrze. Świetny sposób na marnowanie amunicji!" % myself.name); myself.tired()
    else:
      if myself.inflict_dmg(myself.enemy):
        print ("%s pokonał %s!" % (myself.name, myself.enemy.name))
        myself.enemies_defeated +=1
        myself.enemy = None
        myself.state = 'normal'
        eloot = randint(0, 25)
        myself.loot = eloot + myself.loot
        print ("Twój martwy przeciwnik miał przy sobie %d kasy. Miał...Hehe " %(eloot))
        eammo = randint (0, 5)
        myself.ammo = eammo + myself.ammo
        print ("Twój martwy przeciwnik miał przy sobie %d amunicji. Miał...Hehe" %(eammo))
        aidp = randint(0, 4)
        if aidp == 0:
          myself.aid = myself.aid +1
          print ("Ooo! Udało Ci się zdobyć apteczkę!")
        if myself.enemies_defeated + randint(0,int(myself.health_max)/2) >= myself.health_max:
          myself.health = myself.health + 1
          myself.health_max = myself.health_max + 1
          myself.enemies_defeated = 0
          print ("%s czuje się wypełniony DETERMINACJĄ po swoim zwycięstwie i zdobywa lvl!" % myself.name)
      else: myself.enemy_attacks()
  def search(myself):
    if found==True:
      statement = addon1.search_location(myself)
    else:
      print ("Ta opcja jest dostepna tylko w dodatku!")
    if statement != "":
      myself.state = "fight"
      myself.enemy = Enemy(myself,statement)
      print ("Przeciwnik ma %s życia" % myself.enemy.health)
  def enemy_attacks(myself):
    if myself.enemy.inflict_dmg(myself): print ("%s został brutalnie pokonany przez %s!!!\nPrzegrałeś!\n" %(myself.name, myself.enemy.name))
  def save_game(myself):
    if(myself.state == 'normal'):
      myself.toJson()
      print ("Pomyślnie zapisano plik!")
    else:
      print ("Nie możesz być w trakcie walki przy zapisywaniu gry!")


CommandsList = {
  'pomoc': Player.help,
  'zapisz': Player.save_game,
  'wyjdz': Player.quit,
  'status': Player.status,
  'zwiedzaj': Player.explore,
  'przeszukaj': Player.search,
  'atakuj': Player.attack,
  'odpocznij': Player.rest,
  'uciekaj': Player.flee,
  'kup1': Player.item1,
  'kup2': Player.item2,
  'apteczka': Player.aid,
  'sprzedaj': Player.sell
  }

  #Glowna funkcja - startujemy gre
parser = argparse.ArgumentParser(description="Adventurez - mała, fabularna gra RPG w świecie Adwenturnii. \
 Rozgrywka polega na zwiedzaniu i walczeniu z potworami, wszystkie zdarzenia są losowane. Gra posiada 2 opcje - \
 zapisania stanu gry do pliku SAVE (podczas rozgrywki) oraz załadowania pliku SAVE (--load) w celu wznowienia rozgrywki.\
 Pozostałe opcję są in-game, tzn wpisujemy je w linię komend w grze. Komendy są opisane po wywołaniu 'Pomoc'")
parser.add_argument("--load", action="store", dest='save_file', help='Load game')
args = parser.parse_args()
if (args.save_file is not None):
  save_file = args.save_file
if save_file != "":
  filename = save_file
  with open (filename) as json_file:
    data = json.load(json_file)
    p = Player()
    p.name = data["name"]
    p.health = data["health"]
    p.health_max = data["max_health"]
    p.ammo = data["ammo"]
    p.loot = data["money"]
    p.aid = data["aid"]
    p.merchstate = data["merch"]
else:
  p = Player()
  print ("Witaj w grze Adventurez! Aby poznać dostępne komendy wpisz 'pomoc'")
  print ("Najpierw jednak, nazwij swojego bohatera!")
  while(p.name==""):
    p.name = input()
    if(p.name == ""):
      print("No nazwij go jakoś porządnie...")
  print ("%s wyrusza na swoja przygodę w Adwenturni!" % p.name)

while(p.health > 0):
  line = input("Co robisz?: ").lower()
  args = line.split()
  if len(args) > 0:
    commandFound = False
    for c in CommandsList.keys():
      if args[0] == c[:len(args[0])]:
        CommandsList[c](p)
        commandFound = True
        break
    if not commandFound:
      print ("Nie ma takiej komendy, nie wiem, co mam robić...")
