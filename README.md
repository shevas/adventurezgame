# Adventurez - Python Script

UŻYWANIE
--------
Skrypt Adventurez to rodzaj gry przygodowej/RPG 'terminalowej', gdzie rozgrywka polega głównie na walczeniu z rożnymi potworami,
kupowaniu przedmiotów (apteczek/nowej amunicji) i zdobywaniu coraz wyższego poziomu (maksymalnego życia).
Wszystkie zdarzenia (pojawienie się potwora, powrót sprzedawcy, zmęczenie) oraz walki (uderzenia potwora i gracza) są losowane 
przy pomocy modułu 'random' i funkcji 'randomint'.
Wszystkie komendy do gry wpisywane są już po uruchomieniu skryptu - więc sam skrypt wystarczy uruchomić zwykłym poleceniem.
Gra posiada dodatek Addon1 (w folderze Addons), który udostępnia opcję "przeszukaj". Jeśli dodatek nie zostanie załadowany poprawnie, opcja ta jest niedostępna.

### Uruchomienie:
```angular2html
python Adventurez.py 
```

Z dostępnych opcji jest tylko -h/--help (który wypisuje opis skryptu oraz instrukcję używania), oraz --load <file>, który załadowuje plik
z zapisanym stanem gry.
```angular2html
python Adventurez -h/--help #Wypisuje helpa
```
```angular2html
python Adventurez --load <file_name> #Załadowuje grę z pliku (zwykły pliczek JSONowy z odpowiednimi wartościami)
```

TIPSY
-----
Kilka podpowiedzi jak zagrać w Adwenturez i nie przegrać od razu :)
- Zwiedzanie: podróżujemy po Adwenturni, zwiedzając różne lokalizacje. Mamy szansę na pojawienie się potwora, który nas atakuje
			  albo sprzedawcy, u którego możemy kupić Apteczki (uleczające +2 życia) albo Amunicję (potrzebną do atakowania potworów)
- Atakuj: atakujemy potwora podczas walki. Jeśli użyjemy tego nie podczas walki, marnujemy strzał!
- Apteczka: używamy apteczki, aby uleczyć sobie +2 życia
- Odpocznij: odpoczywamy dzięki czemu dochodzi nam +1 życia (Uwaga! Nie można odpoczywać za dużo! Jeśli będziemy cały czas odpoczywać
			 i przekroczymy nasze maskymalne życie to tracimy -1 życia). Podczas odpoczynku mogą nas zaatakować potwory!
- Kup1/Kup2: kupujemy 1 lub 2 przedmiot (czyli apteczkę lub amunicję) u sprzedawcy. Jeśli sprzedawca się nie pojawił, te opcje nic nie dają
- Uciekaj: próbujemy uciec od potwora, który nas zaatakował (jeśli się nie uda, potwór nas atakuje w tej turze!)
- Przeszukaj: [OPCJA DOSTĘPNA TYLKO PRZY ZAŁADOWANYM DODATKU ADDON1] przeszukuje daną lokalizację 
			  (scenariusze:
				- Dodanie +1 życia
				- Dodanie +3 amunicji
				- Walka z silnym potworem
				- Brak akcji)
			  Uwaga! Im częciej przeszukujemy daną lokalizację, tym większa szansa na 'Brak akcji'
- Sprzedaj: sprzedaje coś ze swoich przedmiotów, aby pozyskać więcej pieniędzy
- Status: w każdej chwili sprawdzamy nasze obecne statystyki (życie, ilość pieniędzy, ilość apteczek, ilość amunicji, )

Losowe akcje w grze:
- Atak potwora: musimy wtedy walczyć z danym potworem lub uciec od niego (możemy również sprawdzić nasz status
lub uleczyć się, pozostałe akcje są niemożliwe)
- Zmęczenie: zmęczenie skutkuje obniżeniem życia o -1
- Pojawienie się sprzedawcy: mamy wtedy możliwosć kupienia/sprzedania przedmiotów (sprzedawca jest
dostępny do momentu kolejnego wywołania akcji 'zwiedzaj')

